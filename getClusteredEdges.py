#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import sys
import coexpressionTools as cTools


class Options(object):
    expression_files = []
    cluster_file = []
    out_file = ''
    forbidden_file = ''
    verbose = False


def print_help():
    print('''
usage:
getClusteredEdges.py expressionFile1 [expressionFile2 ...] clusterGenesFile outputEdgesFile [options]

May input multiple expressionFiles.
clusterFile is must be a one column text file with a cluster's genes.

Optional files:
    -f / --forbid [file]        One column text file. All genes in this file will be ignored.
    
Optional flags:
    -v / --verbose
    -h
''')
    return


def parse_input(argv):
    """
    Parses input. Stores in Options object.
    
    Args:
        argv (iterable): 
    """
    files_list = []

    in_options = False

    for n, arg in enumerate(argv):
        if arg[0] == '-':                                 # Options
            in_options = True
            if arg == '-h':
                print_help()
                sys.exit()
            elif arg == '-v' or arg == '--verbose':
                Options.verbose = True
            elif arg == '-f' or arg == '--forbid':
                Options.forbidden_file = argv[n+1]
        elif not in_options:                                # Input
            files_list.append(argv[n])

    Options.expression_files = files_list[:-2]
    Options.cluster_file = files_list[-2]
    Options.out_file = files_list[-1]
    return


#################
# MAIN PIPELINE #
#################
def main(argv):
    # INPUT
    if len(sys.argv) < 2:
        print_help()
        sys.exit()
    else:
        parse_input(argv)

    cluster_genes = cTools.get_one_column_genes(Options.cluster_file)
    forbidden_genes = cTools.get_one_column_genes(Options.forbidden_file)

    exp_dict = cTools.get_expression(Options.expression_files, cluster_genes, forbidden_genes)
    cluster_genes = list(cluster_genes)

    import IPython
    IPython.embed()

    # OUTPUT
    with open(Options.out_file, 'w') as outf:
        outf.write('source,target,score,file\n')

        for cur_file in Options.expression_files:
            for i, gene1 in enumerate(cluster_genes):
                for j, gene2 in enumerate(cluster_genes[i+1:], start=i+1):

                    if gene1 not in exp_dict[cur_file] or gene2 not in exp_dict[cur_file]:
                        continue

                    score = cTools.get_PCC(exp_dict[cur_file][gene1], exp_dict[cur_file][gene2])
                    if score == 'nan':
                        continue

                    new_line = ','.join([gene1, gene2, score, cur_file])
                    outf.write(new_line)
                    outf.write('\n')

    return


if __name__ == "__main__":
    main(sys.argv[1:])
