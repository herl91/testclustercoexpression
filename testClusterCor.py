#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import sys
import pandas as pd
import scipy.stats as stats
import matplotlib.pyplot as plt
import seaborn as sns


class Options(object):
    edges_file = ''
    edges_b_file = ''
    out_file = ''
    verbose = False


def print_help():
    print('''
usage:
testClusterCor.py edgesFile backgroundEdgesFile outFile [options]

Options
    -v / --verbose
    -h
''')
    return


def parse_input(argv):
    """
    Parses input. Stores in Options object.

    Args:
        argv (iterable): 
    """
    Options.edges_file = sys.argv[1]
    Options.edges_b_file = sys.argv[2]
    Options.out_file = sys.argv[3]

    for i, arg in enumerate(argv):
        if arg == '-h':
            print_help()
            sys.exit()
        elif arg == '-v' or arg == '--verbose':
            Options.verbose = True
    return


def get_nodes_from_df(df):
    """
    Gets a set of all nodes from an edges dataframe.

    Args:
        df (pd.DataFrame):

    Returns:
        set: Nodes of input.
    """
    s = set()
    s.update(df.source)
    s.update(df.target)

    return s


#################
# MAIN PIPELINE #
#################
def main(argv):
    if len(sys.argv) < 3:
        print_help()
        sys.exit()

    # ARGS
    parse_input(argv)

    # INPUT
    dist = pd.read_csv(Options.edges_file)
    dist['Distribution'] = 'Cluster'
    if Options.verbose:
        print('Cluster edges loaded.')

    dist_b = pd.read_csv(Options.edges_b_file)
    dist_b['Distribution'] = 'Background'
    if Options.verbose:
        print('Background edges loaded.')

    cluster_size = len(get_nodes_from_df(dist))

    c_scores = dist.score.dropna()
    if len(c_scores) < 20:
        print('Cluster distribution contains less than 20 samples.')

    b_scores = dist_b.score[dist_b.step <= cluster_size].dropna()
    if len(b_scores) < 20:
        print('Background distribution contains less than 20 samples.')

    u = stats.mannwhitneyu(c_scores, b_scores, alternative='greater')[1]
    print('Test results:')
    print('P value =', u)

    dff = pd.concat([dist, dist_b])
    dff['PCC distribution'] = ''
    sns.violinplot(data=dff,
                   x='PCC distribution', y='score', hue='Distribution', inner='quartiles', split=True, cut=0,
                   hue_order=['Background', 'Cluster'])

    plt.ylim(-1.05, 1.05)
    plt.savefig(Options.out_file)
    plt.close()

    return


if __name__ == "__main__":
    main(sys.argv[1:])
