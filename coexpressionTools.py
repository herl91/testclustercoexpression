import numpy as np
import scipy.stats as stats


def calc_MAD(x):
    """
    Calculate MAD via equation below:
    MAD = median( | Xi - median(X) |  )

    Args:
        x (list): Expression array.

    Returns:
        float: MAD score.
    """
    med = np.median(x)
    x2 = np.absolute(x - med)
    MAD = np.median(x2)
    return MAD


def get_PCC(x, y):
    """
    Calculates Pearson Correlation Coefficient

    Args:
        x (list): Expression array of gene1
        y (list): Expression array of gene2

    Returns:
        string: PCC
    """
    if (calc_MAD(x) == 0) or (calc_MAD(y) == 0):
        score = str(np.nan)
    else:
        score = str(stats.pearsonr(x, y)[0])
    return score


def get_expression(exp_files, only_these_genes, forbidden):
    """
    Parses expression files into a dictionary.

    Args:
        exp_files (list):
        only_these_genes (set): Only these genes will be outputted to the dict.
        forbidden (set): These genes will be ignored.

    Returns:
        dict:
    """
    exp_dict = {}
    for cur_file in exp_files:
        exp_dict[cur_file] = {}

        with open(cur_file) as f:
            i = 0
            for line in f:
                if line.startswith('#'):  # Ignore comments
                    continue
                if i == 0:  # Ignore header
                    i += 1
                    continue

                line_array = line.rstrip().split(',')
                gene = line_array[0]

                if only_these_genes and gene not in only_these_genes:
                    continue
                if forbidden and gene in forbidden:
                    continue

                try:
                    exp = map(float, line_array[1:])
                except ValueError:
                    print('File ' + cur_file + ' has a line not properly formatted, which will be skipped:\n' + line)
                    continue

                exp_dict[cur_file][gene] = exp

    return exp_dict


def get_one_column_genes(fname):
    """
    Gets set of genes that will not be used.

    Returns:
        set:
    """
    if fname:
        return set(np.genfromtxt(fname,
                                 dtype='str', comments='#', delimiter=',', skip_header=0, usecols=0))
    else:
        return {}
