# ABOUT #
These scripts can be used to perform the Mann-Whitney U test on BGCs with the
alternative hypothesis that the co-expression of genes in a BGC is stochastically
greater than the co-expression of the background.

The background distribution is created with a sliding window method.

For more information on this method, read our paper at:

https://www.ncbi.nlm.nih.gov/pubmed/28453650

PMID: 28453650

DOI: 10.1093/nar/gkx305

# HOW TO #
1. Use *getClusteredEdges.py* to create the BGC PCC distribution.
2. Use *getBackgroundEdges.py* to create the background distribution. The max
step-size must be equal or larger than the cluster size.
3. Use *testClusterCor.py*

# NOTES #
All expression files must be in csv format, with genes as rows and samples as columns.

# USAGE #
### getClusteredEdges.py ###
    getClusteredEdges.py expressionFile1 [expressionFile2 ...] clusterGenesFile outputEdgesFile [options]
    
    May input multiple expressionFiles.
    clusterFile is must be a one column text file with a cluster's genes.
    
    Optional files:
        -f / --forbid [file]        One column text file. All genes in this file will be ignored.
        
    Optional flags:
        -v / --verbose
        -h

### getBackgroundEdges.py ###
    getBackgroundEdges.py expressionFile1 [expressionFile2 ...] outputEdgesFile [options]
    
    May input multiple expressionFiles.
    The genes in the first expressionFile are assumed to be ordered and in the same record/scaffold/contig/chromosome.
    If this is not the case, use the option -o / --order to use a file with the ordered genes.
    In the order file, empty lines denote the start of a new record/scaffold/contig/chromosome.
    
    Other input (mandatory):
        -m / --max_steps [int]      Max co-expression steps apart to be calculated.
                                    Cannot be zero, and ideally larger than 2.
    
    Optional files:
        -f / --forbid [file]        One column text file. All genes in this file will be ignored.
        -o / --order [file]         One column text file of genes in order. Record breaks are denoted by an empty line.
        
    Optional flags:
        -v / --verbose
        -h

### testClusterCor.py ###
    testClusterCor.py edgesFile backgroundEdgesFile outFile [options]
    
    Options
        -v / --verbose
        -h