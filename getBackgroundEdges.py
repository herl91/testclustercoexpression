#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import sys
import numpy as np
import coexpressionTools as cTools


class Options(object):
    expression_files = []
    out_file = ''
    order_file = ''
    max_steps = 0
    forbidden_file = ''
    verbose = False


def print_help():
    print('''
usage:
getBackgroundEdges.py expressionFile1 [expressionFile2 ...] outputEdgesFile [options]

May input multiple expressionFiles.
The genes in the first expressionFile are assumed to be ordered and in the same record/scaffold/contig/chromosome.
If this is not the case, use the option -o / --order to use a file with the ordered genes.
In the order file, empty lines denote the start of a new record/scaffold/contig/chromosome.

Other input (mandatory):
    -m / --max_steps [int]      Max co-expression steps apart to be calculated.
                                Cannot be zero, and ideally larger than 2.

Optional files:
    -f / --forbid [file]        One column text file. All genes in this file will be ignored.
    -o / --order [file]         One column text file of genes in order. Record breaks are denoted by an empty line.
    
Optional flags:
    -v / --verbose
    -h
''')
    return


def parse_input(argv):
    """
    Parses input. Stores in Options object.
    
    Args:
        argv (iterable): 
    """
    files_list = []

    in_options = False

    for n, arg in enumerate(argv):
        if arg[0] == '-':                                 # Options
            in_options = True
            if arg == '-h':
                print_help()
                sys.exit()
            elif arg == '-v' or arg == '--verbose':
                Options.verbose = True
            elif arg == '-f' or arg == '--forbid':
                Options.forbidden_file = argv[n+1]
            elif arg == '-o' or arg == '--order':
                Options.order_file = argv[n+1]
            elif arg == '-m' or arg == '--max_steps':
                try:
                    Options.max_steps = int(argv[n+1])
                except ValueError:
                    print('Steps is not a number')
                    print_help()
                    sys.exit()
        elif not in_options:                                # Input
            files_list.append(argv[n])

    Options.expression_files = files_list[:-1]
    Options.out_file = files_list[-1]

    if Options.max_steps == 0:
        print('max_steps has not been inputted, or is 0 (non-valid).')
        sys.exit()

    return


def get_order():
    """
    Gets the order of genes either from optional input, or first expression file.
    
    Returns:
        list: Ordered list of genes.
    """
    if Options.order_file:
        order = []
        with open(Options.order_file) as f:
            for line in f:
                order.append(line.rstrip())
        return order
    else:
        return np.genfromtxt(Options.expression_files[0],
                             dtype='str', comments='#', delimiter=',', skip_header=1, usecols=[0])


#################
# MAIN PIPELINE #
#################
def main(argv):
    # INPUT
    if len(sys.argv) < 2:
        print_help()
        sys.exit()
    else:
        parse_input(argv)

    ordered_genes = get_order()

    forbidden_genes = cTools.get_one_column_genes(Options.forbidden_file)

    exp_dict = cTools.get_expression(Options.expression_files, set(), forbidden_genes)

    # OUTPUT
    with open(Options.out_file, 'w') as outf:
        outf.write('source,target,score,step,file\n')

        for cur_file in Options.expression_files:
            for i, gene1 in enumerate(ordered_genes):
                if not gene1:                       # This means there is a record break.
                    continue
                for j, gene2 in enumerate(ordered_genes[i+1:], start=i+1):
                    step = j - i
                    if not gene2:                   # This means there is a record change here. break = Change gene1.
                        break
                    if step > Options.max_steps:    # We've gotten too far. break = Change gene1.
                        break

                    if gene1 not in exp_dict[cur_file] or gene2 not in exp_dict[cur_file]:
                        continue

                    score = cTools.get_PCC(exp_dict[cur_file][gene1], exp_dict[cur_file][gene2])
                    if score == 'nan':
                        continue

                    step = str(step)
                    new_line = ','.join([gene1, gene2, score, step, cur_file])
                    outf.write(new_line)
                    outf.write('\n')

    return


if __name__ == "__main__":
    main(sys.argv[1:])
